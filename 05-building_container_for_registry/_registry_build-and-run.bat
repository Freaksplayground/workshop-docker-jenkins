docker container stop ping-pong
docker container rm ping-pong
docker image rm localhost:4000/ping-pong:lite
docker build -t localhost:4000/ping-pong:lite .
docker push localhost:4000/ping-pong:lite
docker run -d -p 80:80 --name=ping-pong localhost:4000/ping-pong:lite
