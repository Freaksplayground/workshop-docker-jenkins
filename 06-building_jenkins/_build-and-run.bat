docker container stop jenkins-docker
docker container rm jenkins-docker
docker image rm jenkins-docker
docker build -t jenkins-docker .
docker run -p 9080:8080 -p 5000:5000 -d -v /var/run/docker.sock:/var/run/docker.sock -v jenkins_home:/var/jenkins_home --name jenkins-docker jenkins-docker 
