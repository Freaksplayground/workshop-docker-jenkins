# workshop-docker-jenkins

### 01 Container run/stop/remove

- How to run, stop and remove containers.

### 02 Building a Container

- Building our own container using docker commands as well as docker compose to illustrate the pros and cons of both.

### 03 Multi Stage Builds

- Honey, I shrunk the container!

### 04 Deploying a Registry

- Deploying a custom registry to share images across hosts.

### 05 Building Container for Registry

- How to create and push the container to our registry.

### 06 Building Jenkins

- Creating our own version of jenkins with docker access.

### 07 Jenkins Job

- Create a CD cycle for our application.
