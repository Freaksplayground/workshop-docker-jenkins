package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	//default router
	router := gin.Default()
	//api group
	api := router.Group("/api")
	//handler for requests
	api.GET("/ping", pong)
	//start the webserver
	router.Run(":80")
}

func pong(c *gin.Context) {
	c.JSON(http.StatusOK, "pong")
}
